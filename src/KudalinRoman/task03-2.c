#define _CRT_SECURE_NO_WARNINGS
#define N 20
#include <stdio.h>
#include <time.h>
int max=0, ch=0;
void sequenceLength(int *arr, int size)
{
	int i=0, count=1;
	for (i = 0; i < size-1;i++)
	{
		if (arr[i] == arr[i + 1])
		{
			count++;
			if (count >= max)
			{
				ch = arr[i];
				max=count;
			}
		}
		else
			count=1;
	}
}
int main()
{
	int i=0;
	int arr[N];
	srand(time(0));
	for (i=0; i<N; i++)
		arr[i] = rand()%10;
	sequenceLength(arr, N);
	printf("Max length = %d\n", max);
	for (i=0; i<max-1; i++)
		printf("%d,", ch);
	printf("%d\n", ch);
	return 0;
}