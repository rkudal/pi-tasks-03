#define N 20
#include <stdio.h>
#include <time.h>
int main()
{
	int digit = 0, i = 0, sum = 0, number = 0, currentValue = 0;
	int arr[73] = { 0 };
	clock_t start, finish;
	double t;
	for (digit = 1; digit<9; digit++)
	{
		for (i = 0; i<digit; i++)
			number = number * 10 + 9;
		start = clock();
		for (i = 0; i <= number; i++)
		{
			currentValue = i;
			while (currentValue)
			{
				sum += currentValue % 10;
				currentValue /= 10;
			}
			arr[sum]++;
			sum = 0;
		}
		finish = clock();
		t = (double)(finish - start) / CLOCKS_PER_SEC;
		printf("%d - %lf\n", digit, t);
		number = 0;
		currentValue = 0;
	}
	return 0;
}