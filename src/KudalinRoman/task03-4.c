#define _CRT_SECURE_NO_WARNINGS
#define N 20
#include <stdio.h>
#include <time.h>
int i=0;
int maxNum(int *arr, int size)
{
	int max=-200;
	for (i=0; i<size; i++)
		if (arr[i]>max)
			max = arr[i];
	return max;
}
int minNum(int *arr, int size)
{
	int min=200;
	for (i=0; i<size; i++)
		if (arr[i]<min)
			min = arr[i];
	return min;
}
float avgNum(int *arr, int size)
{
	float avg=.0f;
	float sum=.0f;
	for (i=0; i<size; i++)
		sum += arr[i];
	avg=sum/N;
	return avg;
}
int main()
{
	int arr[N];
	srand(time(0));
	for (i=0; i<N; i++)
	{
		arr[i] = rand()%(100-(-100)+1)+(-100);
		printf("%d ", arr[i]);
	}
	printf("\nMax = %d\nMin = %d\nAvg = %4.2f\n", maxNum(arr, N), minNum(arr, N), avgNum(arr, N));
	return 0; 
}
